﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class Studen : HumenInAcademi, IStuden
    {

        public string FamilyStatus { get; set; }
        public int CorePoints { get; set; }
        public int ExtraPoints { get; set; }
        public List<Course> CurrentCorses { get; set; }
        public string FacultyName { get; set; }
        public Enum route { get; set; }
        public Enum profession { get; set; }

        public Studen(string firstname, string lastname, int id, DateTime birthday, string familystatus)
            : base(firstname, lastname, id, birthday)
        {
            this.FamilyStatus = familystatus;
            CurrentCorses = new List<Course>();
            this.CorePoints = 0;
            this.ExtraPoints = 0;
        }

        public Studen()
        { }

    }
}
