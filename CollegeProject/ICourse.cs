﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public interface ICourse
    {
        Teacher Teacher_ { get; set; }
        CLass Class_ { get; set; }

        int Houres { get; }

        int Points { get; set; }
        List<string> Subjects { get; set; }
        Guid ID { get; set; }
        string FacultyName { get; set; }
        string CourseName { get; set; }
    }
}
