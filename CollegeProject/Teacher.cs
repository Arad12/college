﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class Teacher : HumenInAcademi
    {
        public List<string> SubjectsICanTeach { get; set; }

        public bool OnVecation { get; set; }
        public Teacher(string firstname, string lastname, int id, DateTime birthdate) : base(firstname, lastname, id, birthdate) { SubjectsICanTeach = new List<string>(); OnVecation = false; }

        public Teacher() { }

        public void AddSubjectsToTeach()
        {
            bool can = true;
            while (can)
            {
                Console.WriteLine("Add a subject that I can teach:");
                SubjectsICanTeach.Add(Console.ReadLine());
                Console.WriteLine("Other Subjects? (yes/no)");
                var answer = Console.ReadLine().ToLower();
                if (answer == "no")
                    can = false;
            }
        }
    }
}
