﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class CLass : CommonFactor, IClass
    {
        public string BuildingName { get; set; }

        public int ClassNumber { get; set; }

        public int StudentCapacity { get; set; }

        public int CurrentStudentsNumber { get; set; }

        public CLass(int classnumber, int studentcapacity, string buildingname)
        {
            this.StudentCapacity = studentcapacity;
            this.ClassNumber = classnumber;
            this.BuildingName = buildingname;
            this.CurrentStudentsNumber = 0;
        }

        public int AvailableSits()
        {
            return this.StudentCapacity - this.CurrentStudentsNumber;
        }

        public override string ReturnMyName()
        {
            return $"Building name: {this.BuildingName} cluss number: {this.ClassNumber.ToString()}" ;
        }

    }
}
