﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class College : ICollege
    {
        Dictionary<int, Tuple<string, Action>> OptionList;
        public List<Course> AllCoursesInCollege { get; private set; }
        public List<Studen> AllStudents { get; private set; }
        public List<Teacher> AllTeachers { get; private set; }
        public List<Faculty> FacultiesInCollege { get; private set; }
        public List<CLass> AllClasses { get; private set; }
        public string Name { get; set; }
        public Enum Profession { get; private set; }
        public Enum Routes__ { get; private set; }

        public College(string name)
        {
            this.Name = name;
            this.AllStudents = new List<Studen>();
            this.AllClasses = new List<CLass>();
            this.AllTeachers = new List<Teacher>();
            this.AllCoursesInCollege = new List<Course>();
            this.Profession = new Professions.Professionsֹ_();
            this.Routes__ = new Routes.ROutes();
            this.FacultiesInCollege = new List<Faculty>();
            OptionList = new Dictionary<int, Tuple<string, Action>>();
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___("change availabilty of a teacher enter 1"), ChangeAvailabilityOfTeacher));
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___("add humen to a college enter 2"), AddHumenToCollege));
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___("add new faculty enter 3"), CreatNewFaculty));
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___(" add new class enter 4"), CreatNewClass));
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___("add new course enter 5"), AddNewCourse));
            OptionList.Add(OptionList.Count + 1, new Tuple<string, Action>(ReturnIfYouWantTo___("register student to a course enter 6"), RegisterStudentToACourse));
            SecretaryOptions();
        }



        public string ReturnIfYouWantTo___(string continue_)
        {
            return $"If you want to{continue_}";
        }

        public void SecretaryOptions()
        {
            foreach(var option in OptionList)
            {
                Console.WriteLine(option.Value.Item1);
            }
            Console.WriteLine("choose an option");
            var answer = int.Parse(Console.ReadLine());
            while(answer > OptionList.Count && answer<OptionList.Count)
            {
                Console.WriteLine("choose an option");
                answer = int.Parse(Console.ReadLine());
            }
            OptionList[answer].Item2.Invoke();

        }

        public void RecognizeHumenByID<T>(List<T> humenlist, int id, ref HumenInAcademi humen) where T : HumenInAcademi
        {
            foreach (var humen_ in humenlist)
            {
                if (humen_.ID == id)
                {
                    humen = humen_;
                    break;
                }
            }
        }

        public void ChangeAvailabilityOfTeacher()
        {
            var id = EnterAndReturnYourID();
            HumenInAcademi teacher_ = new Teacher();
            RecognizeHumenByID(AllTeachers, id, ref teacher_);
            Teacher teacher = (Teacher)teacher_;
            if (teacher.OnVecation)
            {
                teacher.OnVecation = false;
            }
            else
                teacher.OnVecation = true;
            SecretaryOptions();
        }

        public void AddHumenToCollege()
        {
            string firstname, lastname;
            int id, answer = -1;
            DateTime birthdate;
            bool StudentOrTeacher = true;
            while (StudentOrTeacher)
            {
                Console.WriteLine("If you are a new student, enter 1, if you are a new teacher, enter 0");
                answer = int.Parse(Console.ReadLine());
                if (answer == 1 || answer == 0)
                    StudentOrTeacher = false;
            }
            if (answer == 0)
            {
                EnterBasicHumenDetails("teacher", out firstname, out lastname, out id, out birthdate);
                this.AllTeachers.Add(new Teacher(firstname, lastname, id, birthdate));
                AllTeachers[AllTeachers.Count - 1].AddSubjectsToTeach();
            }
            else if (answer == 1)
            {
                EnterBasicHumenDetails("student", out firstname, out lastname, out id, out birthdate);
                Console.WriteLine("Enter your family status");
                string familystatus = Console.ReadLine();
                Console.WriteLine("Enter the number of your route. \nThe offer routes are:");
                EnumPrinter<Routes.ROutes>();
                int route = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter the number of your profession. \nThe offer routes are:");
                EnumPrinter<Professions.Professionsֹ_>();
                int profession = int.Parse(Console.ReadLine());
                var student = new Studen(firstname, lastname, id, birthdate, familystatus);
                //           student.profession = ChooseOptionFromEnum<Professions.Professionsֹ_>(profession);
                student.profession = (Professions.Professionsֹ_)profession;
                student.route = (Routes.ROutes)route;
                this.AllStudents.Add(student);
            }
            SecretaryOptions();
        }

        public DateTime EnterDate()
        {
            Console.WriteLine("Enter a year of your birthday");
            int year = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter a month of your birthday");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter a day of your birthday");
            int day = int.Parse(Console.ReadLine());
            return new DateTime(year, month, day);
        }

        public void EnumPrinter<T>() where T : Enum
        {
            var i = 1;
            foreach (var t in (T[])Enum.GetValues(typeof(T)))
            {
                Console.WriteLine((i).ToString() + " - " + t.ToString());
                i += 1;
            }
        }

      /*  public Enum ChooseOptionFromEnum<T>(int OptionNumber) where T : Enum
        {
            var option = (T)Enum.ToObject(typeof(T), OptionNumber);
            return (T)option;
        }*/

        public void EnterBasicHumenDetails(string TeacherOrStudent, out string firstname, out string lastname, out int id, out DateTime birthdate)
        {
            Console.WriteLine($"Enter {TeacherOrStudent}'s fisrt name");
            firstname = Console.ReadLine();
            Console.WriteLine($"Enter {TeacherOrStudent}'s last name");
            lastname = Console.ReadLine();
            Console.WriteLine($"Enter {TeacherOrStudent}'s ID");
            id = int.Parse(Console.ReadLine());
            birthdate = EnterDate();
        }
        public void CreatNewFaculty()
        {
            Console.WriteLine("Enter this new faculty name");
            string name = Console.ReadLine();
            this.FacultiesInCollege.Add(new Faculty(name));
            SecretaryOptions();
        }

        public void CreatNewClass()
        {
            Console.WriteLine("Enter the Building name where the class is.");
            string BuildingName = Console.ReadLine();
            Console.WriteLine("Enter a class number");
            int ClassNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the students capacity in the class");
            int StudentsCapacity = int.Parse(Console.ReadLine());
            this.AllClasses.Add(new CLass(ClassNumber, StudentsCapacity, BuildingName));
            SecretaryOptions();
        }

        public void AddNewCourse()
        {
            Console.WriteLine("Enter this course name");
            string CourseName = Console.ReadLine();
            Console.WriteLine("Enter value points for this course");
            int Points = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter how long the course is? (houres)");
            int Houres = int.Parse(Console.ReadLine());
            Console.WriteLine("When it start");
            var start = EnterDate();
            Console.WriteLine("When it end");
            var end = EnterDate();
            CLass class__ = (CLass)PickAVariableFromAList(this.AllClasses, "class");
            Teacher Teacher_ = (Teacher)PickAVariableFromAList(this.AllTeachers, "teacher");
            var NewCourse = new Course(Points, Teacher_, class__, CourseName, Houres, start, end);
            NewCourse.AddSubjectToThisCourse();
            AddCourseToFaculty(NewCourse);
            this.AllCoursesInCollege.Add(NewCourse);
            SecretaryOptions();
        }

        public CommonFactor PickAVariableFromAList<T>(List<T> kindoflist, string WhatInTheList)
            where T : CommonFactor
        {
            int PlaceInList = -1;
            var Argument = new CommonFactor();
            while ((PlaceInList < 0) || (PlaceInList > kindoflist.Count))
            {
                ShowListComponentsNames(kindoflist);
                Console.WriteLine($"Pick a number which describes a {WhatInTheList}. Enter the number near he's name.");
                PlaceInList = int.Parse(Console.ReadLine());
                if (kindoflist.Count >= PlaceInList && PlaceInList >= 0)
                {
                    Argument = kindoflist[PlaceInList];
                }
            }
            return Argument;
        }

        public void AddCourseToFaculty(Course course)
        {
            Faculty faculty = (Faculty)PickAVariableFromAList(this.FacultiesInCollege, "faculty");

            course.FacultyName = faculty.Name;
            faculty.CoursesInFaculty.Add(faculty.CoursesInFaculty.Count + 1, course);
        }

        public void ShowListComponentsNames<T>(List<T> KindOfList)
            where T : CommonFactor
        {
            foreach (var argument in KindOfList)
                Console.WriteLine($"{KindOfList.IndexOf(argument)} - {argument.ReturnMyName()}");
        }

        public List<Course> ReturnWhichCourseAreAvailable()
        {
            var AvailableCourses_ = new List<Course>();
            foreach(var t in AllCoursesInCollege)
            {
                if (t.Class_.AvailableSits() > 0)
                    AvailableCourses_.Add(t);
            }
            return AvailableCourses_;
            /*
             * why the linq is not working?
             * IOrderedEnumerable<Course>
            var AvailableCourses_ = from Cours_e in AllCoursesInCollege
                                    where Cours_e.Class_.AvailableSits() > 0
                                    orderby Cours_e.CourseName
                                    select Cours_e;*/
        }

        public int EnterAndReturnYourID()
        {
            Console.WriteLine("Enter your ID");
            int id = int.Parse(Console.ReadLine());
            return id;
        }

        public void RegisterStudentToACourse()
        {
            int id = EnterAndReturnYourID();
            var AvailableCourses_ = ReturnWhichCourseAreAvailable();
            for (int i = 0; i < AvailableCourses_.Count; i++)
            {
                Console.WriteLine($"{i}\nCourse name: {AvailableCourses_[i].CourseName}\nSubjects in this course:");
                AvailableCourses_[i].SubjectsInCourse();
            }
            Console.WriteLine("To choose a course to register to, enter number above it.");
            int ChoosenCourseNumber = int.Parse(Console.ReadLine());
            HumenInAcademi student__ = new Studen();
            RecognizeHumenByID(AllStudents,id, ref student__);
            Studen student___ = (Studen)student__;
            student___.CurrentCorses.Add(AvailableCourses_[ChoosenCourseNumber]);
            if (student___.FacultyName == AvailableCourses_[ChoosenCourseNumber].FacultyName)
            {
                student___.CorePoints += AvailableCourses_[ChoosenCourseNumber].Points;
            }
            else
            {
                student___.ExtraPoints += AvailableCourses_[ChoosenCourseNumber].Points;
            }
            AvailableCourses_[ChoosenCourseNumber].Class_.CurrentStudentsNumber += 1;
            SecretaryOptions();
        }
        
    }
}
