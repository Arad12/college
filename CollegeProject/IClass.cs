﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public interface IClass
    {
        string BuildingName { get; set; }
        int ClassNumber { get; set; }
        int StudentCapacity { get; set; }
        int CurrentStudentsNumber { get; set; }
        

    }
}
