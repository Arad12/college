﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class Faculty : CommonFactor, IFaculty
    {

        public string Name { get; set; }
        public Dictionary<int, Course> CoursesInFaculty { get; set; }
        public List<Studen> StudentsInFaculty { get; set; }
        public List<Teacher> Teachers { get; set; }

        public Faculty(string name)
        {
            this.Name = name;
            CoursesInFaculty = new Dictionary<int, Course>();
            this.StudentsInFaculty = new List<Studen>();
            this.Teachers = new List<Teacher>();
        }

        public override string ReturnMyName()
        {
            return this.Name;
        }
    }
}
