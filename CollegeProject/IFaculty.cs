﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public interface IFaculty
    {
        string Name { get; set; }
        Dictionary<int, Course> CoursesInFaculty { get; set; }
        List<Studen> StudentsInFaculty { get; set; }
        List<Teacher> Teachers { get; set; }
    }
}
