﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class Course : CommonFactor, ICourse
    {
        public int Houres { get; }

        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public int Points { get; set; }
        public List<string> Subjects { get; set; }
        public Teacher Teacher_ { get; set; }
        public CLass Class_ { get; set; }
        public Guid ID { get; set; }
        public string FacultyName { get; set; }
        public string CourseName { get; set; }

        public Course(int points, Teacher teacher, CLass class_, string coursename, int houres, DateTime start, DateTime end)
        {
            this.Begin = start;
            this.End = end;
            this.Houres = houres;
            this.Points = points;
            this.Subjects = new List<string>();
            this.Teacher_ = teacher;
            this.Class_ = class_;
            this.CourseName = coursename;
            this.ID = Guid.NewGuid();
            Console.WriteLine(this.ID);
        }

        public override string ReturnMyName()
        {
            return $"Course name: {this.CourseName}.\nCourse teacher: {this.Teacher_.ToString()}\nCourse ID : {this.ID}";
        }

        public void AddSubjectToThisCourse()
        {
            bool More = true;
            while (More)
            {
                Console.WriteLine("Enter a subject");
                this.Subjects.Add(Console.ReadLine());
                Console.WriteLine("If you want to add more subjects, enter 1. Any other answer will stop this function.");
                int answer = int.Parse(Console.ReadLine());
                if (answer != 1)
                    More = false;
            }
        }



        public void SubjectsInCourse()
        {
            foreach (var subject in this.Subjects)
            {
                Console.WriteLine(subject);
            }
        }

    }
}
