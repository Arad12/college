﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public interface IHumenInAcademi
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        int ID { get; set; }
        DateTime BirthDate { get; }
    }
}
