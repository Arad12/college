﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public class HumenInAcademi : CommonFactor, IHumenInAcademi
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public DateTime BirthDate { get; }

        
        public HumenInAcademi(string firstname, string lastname, int id, DateTime birthdate)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.ID = id;
            this.BirthDate = birthdate;
        }

        public HumenInAcademi() { }

        public override string ReturnMyName()
        {
            return $"I am {FirstName} {LastName}";
        }

    }
}
