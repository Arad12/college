﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollegeProject
{
    public interface ICollege
    {
        string Name { get; set; }
        Enum Routes__ { get; }
        Enum Profession { get; }
        List<Faculty> FacultiesInCollege { get; }
        List<Course> AllCoursesInCollege { get; }
        List<Studen> AllStudents { get; }
        List<Teacher> AllTeachers { get; }
        List<CLass> AllClasses { get; }

    }
}
